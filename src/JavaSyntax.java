package src;

public class JavaSyntax {

    public static int fact(int n){
        if (n == 0) return 1;
        return n * fact(n-1);
    }

    public static boolean isPrime(int n){
        return (n%2 == 0) ? true : false;
    }

    public static double calc(int n){

        //in case of wrong input
        if (n==0) return -1;

        //necessary to produce double numbers when performing division operation
        double i,sum = 0.0;
        for (i = 1; i <= n; i++) {
            sum += 1/i;
        }
        return sum;
    }

    public static double getDecimal(double n){

        if (n <= 0) return -1;

        return n - (int) n;
    }


}
