package src.palindrome;

public class Main {

    public static void main(String[] args) {
        String txt = "abBa";
        Palindrome palindrome = new Palindrome(txt);
        if (palindrome.isPalindrome(txt))
            System.out.println("String: "+txt+" is palindrome!");
        else
            System.out.println("String: "+txt+" is not palindrome!");
    }

}
