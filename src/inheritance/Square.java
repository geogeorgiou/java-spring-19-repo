package src.inheritance;

public class Square extends TwoDimensionalShape {

    private double side;

    public Square(double area, double side) {
        super(area);
        this.convex = false;
        this.side = side;
    }

    @Override
    public double getArea() {
        return 4*side;
    }

    public double getSide() {
        return side;
    }
}
