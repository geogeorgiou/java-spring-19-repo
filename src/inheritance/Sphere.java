package src.inheritance;

public class Sphere extends ThreeDimensionalShape {

    private double radius;

    public Sphere(double area, double radius) {
        super(area);
        this.radius = radius;
    }

    public double getArea(){
        return 4*Math.PI*Math.pow(radius,2);
    }

    public double getRadius() {
        return radius;
    }
}
