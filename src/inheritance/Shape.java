package src.inheritance;

public class Shape {

    private double area;

    public Shape(double area) {
        this.area = area;
    }

    public double getArea() {
        return area;
    }
}
