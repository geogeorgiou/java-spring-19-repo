package src.inheritance;

public class Cube extends ThreeDimensionalShape{

    private double side;

    public Cube(double area, double radius) {
        super(area);
        this.side = radius;
    }

    @Override
    public double getArea() {
        return 6*Math.pow(side,2);
    }

    public double getRadius() {
        return side;
    }
}
