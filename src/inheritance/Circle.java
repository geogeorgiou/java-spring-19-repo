package src.inheritance;

public class Circle extends TwoDimensionalShape{

    private double radius;

    public Circle(double area, double radius) {
        super(area);
        this.convex = true;
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return 2*Math.PI*Math.pow(radius,2);
    }

    public double getRadius() {
        return radius;
    }
}
