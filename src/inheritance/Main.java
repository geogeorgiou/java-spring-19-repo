package src.inheritance;

public class Main {

    public static void main(String[] args) {
        Shape sphere = new Sphere(1,3.21f);
        Shape square = new Square(1,2.0f);

        System.out.println(sphere.getArea());
        System.out.println(square.getArea());
    }
}
