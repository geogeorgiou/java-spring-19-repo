package src.inheritance;

public class TwoDimensionalShape extends Shape {

    protected boolean convex;

    public TwoDimensionalShape(double area) {
        super(area);
    }

    public boolean isConvex(){
        return convex;
    }

    public void setConvex(boolean convex) {
        this.convex = convex;
    }
}
