package src.inheritancecontd;

public class Product {

    private double price;
    private String brand;
    private int quantity;

    public Product(double price, String brand, int quantity) {
        this.price = price;
        this.brand = brand;
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
