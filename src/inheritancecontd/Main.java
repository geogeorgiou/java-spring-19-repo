package src.inheritancecontd;

public class Main {

    public static void main(String[] args) {

        Product salable_product = new Product(1.22,"Doritos",3);
        Product non_salable_product = new Product(1.1,"Pringles",3);
        Employee employee = new Employee("Chris","Gavanas",26);
        Shop shop = new Shop("Market In",salable_product,employee);

        if ((shop.getProduct().getBrand()).equals(salable_product.getBrand())){
            if (shop.handleRequest(salable_product)==true)
                System.out.println("Remaining quantity of "+shop.getProduct().getBrand()+" is "+shop.getProduct().getQuantity());
        }

        System.out.println("*********************");

        if ((shop.getProduct().getBrand()).equals(non_salable_product.getBrand())){
            if (shop.handleRequest(non_salable_product)==true)
                System.out.println("Remaining quantity of "+shop.getProduct().getBrand()+" is "+shop.getProduct().getQuantity());
        }
        else
            System.err.println("No reserve on "+non_salable_product.getBrand());


    }

}
