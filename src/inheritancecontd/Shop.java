package src.inheritancecontd;

public class Shop {

    private String name;
    private Product product;
    private Employee employee;

    public Shop(String name, Product product, Employee employee) {
        this.name = name;
        this.product = product;
        this.employee = employee;
    }

    public boolean handleRequest(Product product){
        return employee.sell(product);
    }

    public String getName() {
        return name;
    }

    public Product getProduct() {
        return product;
    }

    public Employee getEmployee() {
        return employee;
    }
}
