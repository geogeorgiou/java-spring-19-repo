package src.inheritancecontd;

public class Employee {

    private String name;
    private String surname;
    private int age;

    public Employee(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public boolean sell(Product product){
        if (product.getQuantity() == 0){
            System.out.println("Product "+ product.getBrand() +" out of stock!");
            return false;
        }
        product.setQuantity(product.getQuantity()-1);
        System.out.println("Successful sale of "+ product.getBrand());
        return true;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }
}
