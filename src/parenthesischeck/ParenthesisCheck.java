package src.parenthesischeck;

import java.util.Stack;

public class ParenthesisCheck {

    public void isCompiled(String str,Stack<Character> stack){

        if (push_to_Stack(str,stack)) {
            //creating counters for left and right parenthesis
            int left=0,right=0;

            //iterate through the stack and check whether the left
            //and right parenthesis match as numbers if not then
            //return false

            while (!stack.empty()){
                char c = stack.pop();
                if (c == '{')
                    left++;
                else if (c == '}')
                    right++;

            }

            if (left==right) System.out.println("Code line is compiled");
            else System.out.println("Code line is not compiled");
        }


    }

    public boolean push_to_Stack(String str,Stack<Character> stack){
        int length = str.length()-1;
        if (length<0) return false;

        for (int i = 0; i < length; i++) {
            stack.push((Character) str.charAt(i));
        }

        return true;
    }


    public static void main(String[] args) {
        Stack<Character> stack = new Stack<Character>();
        String txt = "sth {{ is missing } here";

        ParenthesisCheck prCheck = new ParenthesisCheck();

        prCheck.isCompiled(txt,stack);
    }

}
