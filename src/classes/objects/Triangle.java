package src.classes.objects;

public class Triangle {

    private Point p1,p2,p3;

    public Triangle(Point p1, Point p2, Point p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public double getPerimeter(){
        return getDistance(p1,p2)+getDistance(p2,p3)+getDistance(p3,p1);
    }

    public double getDistance(Point a,Point b){
        return Math.sqrt(Math.pow( a.getX() - b.getX(),2 ) + Math.pow( a.getY() - b.getY(),2));
    }
}
